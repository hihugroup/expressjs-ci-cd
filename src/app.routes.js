const express = require('express')
const router = express.Router()

const welcomeCtrl = require('./controllers/welcome')
router.get('/', welcomeCtrl.ping)
router.get('/ping', (req, res) => req.send('ok'))
router.all('*', welcomeCtrl.notFound)

module.exports = router
