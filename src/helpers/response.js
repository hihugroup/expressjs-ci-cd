const staticCode = require('../app.static')

exports.sendSuccess = (req, res) => (data) => {
    const response = {
        success: true,
        message: staticCode.SUCCESS,
        data: data || {},
    }

    res.status(200).json(response)
}

exports.sendError = (req, res) => (err) => {
    console.log(err)
    const {message, status} = err
    const response = {
        success: false,
        message: message,
        data: null,
    }

    res.status(status || 200).json(response)
}
